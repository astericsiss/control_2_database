package com.example.control_2_DataBase;

import com.example.control_2_DataBase.converter.RequestPersonToEntityConverter;
import com.example.control_2_DataBase.entity.RequestEntity;
import com.example.control_2_DataBase.logic.Person;
import com.example.control_2_DataBase.logic.RequestCredit;
import com.example.control_2_DataBase.repository.RequestRepository;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Control2DataBaseApplicationTests {

    private final List<Person> positiveCreditTestList = Arrays.asList(
            new Person(18, "David", 1800),
            new Person(68, "Igor", 6800));

    private final List<Person> negativeCreditTestList = Arrays.asList(
            new Person(18, "Bob", 1800),
            new Person(18, "Vlad", 4000),
            new Person(16, "Thomas", 1500),
            new Person(44, "Jack", 4401));


    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestPersonToEntityConverter converter;

    @LocalServerPort
    private Integer port;
    private RequestSpecification request;

    @BeforeEach
    public void setup() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @AfterEach
    void delete() {
        requestRepository.deleteAll();
    }

    @Test
    void getIdPersonTest() {
        List<Integer> id = addPersonList(positiveCreditTestList);
        for (int i = 0; i < getAllList().size(); i++) {
            assertEquals(id.get(i), getAllList().get(i).getId());
        }
    }

    @Test
    void getPersonTest() {
        int id = addPerson();
        request.get("/credit/" + id)
                .then().log().all()
                .statusCode(200)
                .body("age", equalTo(68))
                .body("name", equalTo("Igor"))
                .body("sum", equalTo(6800))
                .body("creditEnable", equalTo(true));
    }

    @Test
    void getDataBasePersonTest() {
        setData(negativeCreditTestList);
        List<RequestEntity> requestCredits = requestRepository.findAll().stream()
                .filter(requestEntity -> requestEntity.getUserName().equals("Jack"))
                .collect(Collectors.toList());
        assertEquals(requestCredits.get(0).getUserName(), "Jack");
        assertEquals(requestCredits.get(0).getUserAge(), 44);
        assertEquals(requestCredits.get(0).getUserSum(), 4401);
    }

    @Test
    void getDataBaseListPersonTest() {
        setData(positiveCreditTestList);
        List<RequestCredit> requestCreditList = Arrays.asList(request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/credit/all")
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .as(RequestCredit[].class));
        for (int i = 0; i < requestCreditList.size(); i++) {
            assertEquals(requestCreditList.get(i).getUser(), positiveCreditTestList.get(i));
        }
    }

    @Test
    void getListLengthDataBasePersonTest() {
        setData(positiveCreditTestList);
        List<RequestEntity> requestCredits = requestRepository.findAll();
        assertEquals(requestCredits.size(), 2);
    }

    @Test
    void deletePersonPositiveTest() {
        List<Integer> id = addPersonList(negativeCreditTestList);
        request.delete("/credit/" + id.get(2))
                .then()
                .statusCode(200);
        assertEquals(getAllList().size(), negativeCreditTestList.size() - 1);
    }

    @Test
    void deletePersonNegativeTest() {
        addPersonList(negativeCreditTestList);
        request.delete("/credit/" + 5)
                .then()
                .statusCode(200);
        assertEquals(getAllList().size(), negativeCreditTestList.size());
    }

    @Test
    void getCreditResultPositiveTest() {
        for (Person user : positiveCreditTestList) {
            boolean response = request.baseUri(RestAssured.baseURI)
                    .contentType(ContentType.JSON)
                    .body(user)
                    .post("/credit/result")
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(Boolean.class);
            assertTrue(response);
        }
    }

    @Test
    void getCreditResultNegativeTest() {
        for (Person user : negativeCreditTestList) {
            boolean response = request.baseUri(RestAssured.baseURI)
                    .contentType(ContentType.JSON)
                    .body(user)
                    .post("/credit/result")
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(Boolean.class);
            assertFalse(response);
        }
    }

    @Test
    void getModificationApplicationTest() {
        List<Integer> id = addPersonList(negativeCreditTestList);
        RequestCredit user = new RequestCredit(id.get(2), new Person(20, "Thomas", 1500));
        request.baseUri(RestAssured.baseURI)
                .contentType(ContentType.JSON)
                .when()
                .body(user)
                .put("/credit/modification")
                .then()
                .statusCode(200);
        request.get("/credit/" + id.get(2))
                .then().log().all()
                .body("age", equalTo(20))
                .body("name", equalTo("Thomas"))
                .body("sum", equalTo(1500))
                .body("creditEnable", equalTo(true));
    }

    @Test
    void getCreditListTest() {
        addPersonList(positiveCreditTestList);
        assertEquals(getAllList().size(), positiveCreditTestList.size());
    }

    @Test
    void getCreditListPersonTest() {
        addPersonList(positiveCreditTestList);
        for (int i = 0; i < getAllList().size(); i++) {
            assertEquals(getAllList().get(i).getUser().getSum(), positiveCreditTestList.get(i).getSum());
            assertEquals(getAllList().get(i).getUser().getAge(), positiveCreditTestList.get(i).getAge());
            assertEquals(getAllList().get(i).getUser().getName(), positiveCreditTestList.get(i).getName());
            assertTrue(getAllList().get(i).getUser().isCreditEnable());
        }
    }

    private Integer addPerson() {
        return request.baseUri(RestAssured.baseURI)
                .contentType(ContentType.JSON)
                .when()
                .body(positiveCreditTestList.get(1))
                .get("/credit/request")
                .then()
                .extract()
                .as(Integer.class);
    }

    private List<Integer> addPersonList(List<Person> userList) {
        List<Integer> result = new ArrayList<>();
        for (Person user : userList) {
            result.add(request.baseUri(RestAssured.baseURI)
                    .contentType(ContentType.JSON)
                    .when()
                    .body(user)
                    .get("/credit/request")
                    .then()
                    .extract()
                    .as(Integer.class));
        }
        return result;
    }

    private List<RequestCredit> getAllList() {
        return request.get("/credit/all")
                .then()
                .statusCode(200)
                .extract()
                .as(new TypeRef<>() {
                });
    }

    private void setData(List<Person> personList) {
        for (Person person : personList) {
            RequestEntity requestEntity = converter.convertUser(person);
            requestEntity.setUserSum(requestEntity.getUserSum());
            requestEntity.setUserName(requestEntity.getUserName());
            requestEntity.setUserAge(requestEntity.getUserAge());
            requestRepository.save(requestEntity);
        }
    }
}
