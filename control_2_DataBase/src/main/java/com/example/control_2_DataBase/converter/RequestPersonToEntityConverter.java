package com.example.control_2_DataBase.converter;

import com.example.control_2_DataBase.entity.RequestEntity;
import com.example.control_2_DataBase.logic.Person;
import com.example.control_2_DataBase.logic.RequestCredit;
import org.springframework.stereotype.Service;

@Service
public class RequestPersonToEntityConverter {

    public RequestEntity convertUser(Person user) {
        RequestEntity requestEntity = new RequestEntity();
        requestEntity.setUserAge(user.getAge());
        requestEntity.setUserName(user.getName());
        requestEntity.setUserSum((long) user.getSum());
        requestEntity.setCreditEnable(user.isCreditEnable());
        return requestEntity;
    }

    public RequestCredit convertRequestEntity(RequestEntity requestEntity) {
        RequestCredit requestCredit = new RequestCredit();
        Person user = new Person();
        user.setAge(requestEntity.getUserAge());
        user.setSum(Math.toIntExact(requestEntity.getUserSum()));
        user.setName(requestEntity.getUserName());
        user.setCreditEnable(requestEntity.getCreditEnable());
        requestCredit.setId(requestEntity.getId());
        requestCredit.setUser(user);
        return requestCredit;
    }
    public Person convertEntity(RequestEntity requestEntity) {
        Person user = new Person();
        user.setAge(requestEntity.getUserAge());
        user.setSum(Math.toIntExact(requestEntity.getUserSum()));
        user.setName(requestEntity.getUserName());
        user.setCreditEnable(requestEntity.getCreditEnable());
        return user;
    }
}
