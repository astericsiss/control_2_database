package com.example.control_2_DataBase.logic;

import org.springframework.stereotype.Service;

@Service
public class RequestCredit {
    private int id;
    private Person user;

    public RequestCredit() {

    }

    public RequestCredit(int id, Person user) {
        this.id = id;
        this.user = user;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getUser() {
        return user;
    }

    public void setUser(Person user) {
        this.user = user;
    }
}
