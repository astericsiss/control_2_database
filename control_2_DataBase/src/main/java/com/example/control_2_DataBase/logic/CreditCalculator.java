package com.example.control_2_DataBase.logic;

import org.springframework.stereotype.Service;

@Service
public class CreditCalculator {

    public Person calculateCredit(Person person) {
        boolean result = false;
        String name = "Bob";
        if (person.getAge() >= 18 && !person.getName().equalsIgnoreCase(name) && person.getSum() <= (person.getAge() * 100)) {
            result = true;
        }
        person.setCreditEnable(result);
        return person;
    }
}
