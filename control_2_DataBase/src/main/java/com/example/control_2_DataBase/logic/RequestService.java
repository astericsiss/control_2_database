package com.example.control_2_DataBase.logic;

import com.example.control_2_DataBase.converter.RequestPersonToEntityConverter;
import com.example.control_2_DataBase.entity.RequestEntity;
import com.example.control_2_DataBase.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private RequestPersonToEntityConverter converter;

    @Transactional
    public int saveEntity(Person person) {
        RequestEntity requestEntity = converter.convertUser(person);
        return Math.toIntExact(requestRepository.save(requestEntity).getId());
    }

    @Transactional(readOnly = true)
    public Person getUserRequestById(int id) {
        RequestEntity requestEntity = requestRepository.getById(id);
        return converter.convertEntity(requestEntity);
    }

    @Transactional(readOnly = true)
    public List<RequestCredit> getUserRequests() {
        List<RequestEntity> requestEntities = requestRepository.findAll();
        return requestEntities.stream()
                .map(requestEntity -> converter.convertRequestEntity(requestEntity))
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteEntity(int id) {
        RequestEntity requestEntity = requestRepository.getById(id);
        requestRepository.delete(requestEntity);
    }

    @Transactional
    public void modificationEntity(RequestCredit user) {
        RequestEntity requestEntity = requestRepository.getById(user.getId());
        requestEntity.setUserAge(user.getUser().getAge());
        requestEntity.setUserName(user.getUser().getName());
        requestEntity.setUserSum((long) user.getUser().getSum());
        requestEntity.setCreditEnable(user.getUser().isCreditEnable());
        requestRepository.save(requestEntity);
    }
}
