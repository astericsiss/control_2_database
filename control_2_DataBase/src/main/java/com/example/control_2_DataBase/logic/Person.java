package com.example.control_2_DataBase.logic;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Person {
    private int age;
    private String name;
    private int sum;
    private boolean creditEnable;

    public Person(int age, String name, int sum) {
        this.age = age;
        this.name = name;
        this.sum = sum;
    }
}
