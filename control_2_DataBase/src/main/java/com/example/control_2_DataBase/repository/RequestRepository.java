package com.example.control_2_DataBase.repository;

import com.example.control_2_DataBase.entity.RequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestRepository extends JpaRepository<RequestEntity, Integer> {
}

