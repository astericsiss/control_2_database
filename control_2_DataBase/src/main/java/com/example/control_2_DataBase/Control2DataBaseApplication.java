package com.example.control_2_DataBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Control2DataBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Control2DataBaseApplication.class, args);
	}

}
