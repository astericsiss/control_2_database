package com.example.control_2_DataBase.endpoints;

import com.example.control_2_DataBase.logic.CreditCalculator;
import com.example.control_2_DataBase.logic.Person;
import com.example.control_2_DataBase.logic.RequestCredit;
import com.example.control_2_DataBase.logic.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/credit")
public class CreditEndPoints {

    @Autowired
    private CreditCalculator creditCalculator;

    @Autowired
    private RequestService requestService;


    @GetMapping("/request")
    public int getIDPerson(@RequestBody Person person) {
        creditCalculator.calculateCredit(person);
        return requestService.saveEntity(person);
    }

    @GetMapping(path = "/{id}")
    public Person getPerson(@PathVariable int id) {
        return requestService.getUserRequestById(id);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable int id) {
        requestService.deleteEntity(id);
    }

    @PostMapping("/result")
    public boolean getCreditResult(@RequestBody Person person) {
        if (getCreditList().size() != 0) {
            for (int i = 0; i < getCreditList().size(); i++) {
                if (!(getCreditList().get(i).getUser().getName().equalsIgnoreCase(person.getName()))) {
                    creditCalculator.calculateCredit(person);
                }
            }
        } else {
            creditCalculator.calculateCredit(person);
        }
        return creditCalculator.calculateCredit(person).isCreditEnable();
    }

    @PutMapping("/modification")
    public void getModificationApplication(@RequestBody RequestCredit user) {
        for (int i = 0; i < getCreditList().size(); i++) {
            if (getCreditList().get(i).getId() == user.getId() &&
                    getCreditList().get(i).getUser().getName().equals(user.getUser().getName())) {
                RequestCredit requestCredit = new RequestCredit();
                requestCredit.setId(user.getId());
                creditCalculator.calculateCredit(user.getUser());
                requestCredit.setUser(user.getUser());
                requestService.modificationEntity(requestCredit);
            }
        }
    }

    @GetMapping("/all")
    public List<RequestCredit> getCreditList() {
        return requestService.getUserRequests();
    }
}
